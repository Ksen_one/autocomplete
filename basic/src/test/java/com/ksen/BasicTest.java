package com.ksen;

import org.junit.Test;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintStream;

public class BasicTest {
    @Test
    public void KonturTestFile(){
        try {
            System.setIn(new FileInputStream("../test.in"));
            System.setOut(new PrintStream(new FileOutputStream("../test.out")));
        } catch (IOException e){
            e.printStackTrace();
            return;
        }

        Thread basic = new Thread(new Basic());
        basic.start();
        try {
            basic.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}