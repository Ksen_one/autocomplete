package com.ksen;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Basic implements Runnable {
    public static void main(String[] args) {
        new Thread(new Basic()).start();
    }

    @Override
    public void run() {
        final int adviceCount = 10;
        AutocompleteEngine engine = new AutocompleteEngine(adviceCount);
        String[] prefixList = null;

        try (BufferedReader reader = new BufferedReader(new InputStreamReader(System.in))) {
            int n = Integer.parseInt(reader.readLine());
            for (int i = 0; i < n; i++) {
                String[] splitedLine = reader.readLine().split(" ");
                engine.put(splitedLine[0],Integer.parseInt(splitedLine[1]));
            }
            int m = Integer.parseInt(reader.readLine());
            prefixList = new String[m];
            for (int i = 0; i < m; i++) {
                prefixList[i] = reader.readLine();
            }
        } catch (IOException e) {
            e.printStackTrace();
            System.exit(-1);
        }

        for (String prefix : prefixList) {
            engine.getAdvices(prefix).forEach(System.out::println);
            System.out.println();
        }
    }
}