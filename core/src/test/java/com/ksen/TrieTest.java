package com.ksen;

import com.ksen.data.Trie;
import com.ksen.data.TrieEntry;
import org.junit.Test;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;

import static org.junit.Assert.*;

public class TrieTest {
    @Test
    public void insertNull() {
        Trie trie = new Trie(10);
        assertFalse(trie.put(null));
    }

    @Test
    public void insertEntryWithZeroWeight() {
        Trie trie = new Trie(10);
        assertTrue(trie.put(new TrieEntry("abs", 0)));
    }

    @Test
    public void insertEnrtyWithEmptyTerm() {
        Trie trie = new Trie(10);
        assertFalse(trie.put(new TrieEntry("", 0)));
    }

    @Test
    public void insertEntryWithExistingTerm() {
        Trie trie = new Trie(10);
        trie.put(new TrieEntry("abs", 0));
        assertFalse(trie.put(new TrieEntry("abs", 0)));
    }

    private void initTrieFromTask(Trie trie) {
        trie.put(new TrieEntry("kare", 10));
        trie.put(new TrieEntry("kanojo", 20));
        trie.put(new TrieEntry("karetachi", 1));
        trie.put(new TrieEntry("korosu", 7));
        trie.put(new TrieEntry("sakura", 3));
    }

    private void initOrderTree(Trie trie) {
        trie.put(new TrieEntry("abb", 7));
        trie.put(new TrieEntry("aaa", 3));
        trie.put(new TrieEntry("acc", 3));
        trie.put(new TrieEntry("aab", 3));
        trie.put(new TrieEntry("aba", 3));
        trie.put(new TrieEntry("aca", 7));
        trie.put(new TrieEntry("aac", 3));
        trie.put(new TrieEntry("acb", 7));
    }

    @Test
    public void searchFromTask1() {
        Trie trie = new Trie(10);
        initTrieFromTask(trie);

        String[] expect = {"kanojo", "kare", "korosu", "karetachi"};
        Collection<TrieEntry> advices = trie.searchTermsWithPrefix("k");
        assertEquals(Arrays.toString(expect), Arrays.toString(advices.toArray(new TrieEntry[advices.size()])));
    }

    @Test
    public void searchFromTask2() {
        Trie trie = new Trie(10);
        initTrieFromTask(trie);

        String[] expect = {"kanojo", "kare", "karetachi"};
        Collection<TrieEntry> advices = trie.searchTermsWithPrefix("ka");
        assertEquals(Arrays.toString(expect), Arrays.toString(advices.toArray(new TrieEntry[advices.size()])));
    }

    @Test
    public void searchFromTask3() {
        Trie trie = new Trie(10);
        initTrieFromTask(trie);

        String[] expect = {"kare", "karetachi"};
        Collection<TrieEntry> advices = trie.searchTermsWithPrefix("kar");
        assertEquals(Arrays.toString(expect), Arrays.toString(advices.toArray(new TrieEntry[advices.size()])));
    }

    @Test
    public void searchAbcOrder() {
        Trie trie = new Trie(10);
        initOrderTree(trie);

        String[] expect = {"abb", "aca", "acb", "aaa", "aab", "aac", "aba", "acc"};
        Collection<TrieEntry> advices = trie.searchTermsWithPrefix("a");
        assertEquals(Arrays.toString(expect), Arrays.toString(advices.toArray(new TrieEntry[advices.size()])));
    }

    @Test
    public void searchInEmptyTrie() {
        Trie trie = new Trie(10);

        Collection<TrieEntry> advices = trie.searchTermsWithPrefix("abc");
        assertEquals(Collections.emptySet(), advices);
    }

    @Test
    public void searchEmptyPrefix() {
        Trie trie = new Trie(10);
        initOrderTree(trie);

        Collection<TrieEntry> advices = trie.searchTermsWithPrefix("");
        assertEquals(Collections.emptySet(), advices);
    }

    @Test
    public void searchNotExistPrefix() {
        Trie trie = new Trie(10);
        initOrderTree(trie);

        Collection<TrieEntry> advices = trie.searchTermsWithPrefix("jo");
        assertEquals(Collections.emptySet(), advices);
    }

}