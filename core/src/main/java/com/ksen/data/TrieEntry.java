package com.ksen.data;

public class TrieEntry implements Comparable<TrieEntry> {
    private final String term;
    private final int weight;

    public TrieEntry(String term, int weight) {
        this.term = term;
        this.weight = weight;
    }

    public String getTerm() {
        return term;
    }

    public int getWeight() {
        return weight;
    }

    @Override
    public String toString() {
        return term;
    }

    @Override
    public int compareTo(TrieEntry o) {
        return weight > o.weight ? -1 : weight == o.weight ? alphabeticalCompareTo(o) : 1;
    }

    private int alphabeticalCompareTo(TrieEntry o) {
        return term.compareTo(o.term);
    }
}
