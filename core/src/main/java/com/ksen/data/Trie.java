package com.ksen.data;

import java.util.*;

public class Trie {
    private final int adviceCount;
    private TrieNode root;

    public Trie(int adviceCount) {
        this.root = new TrieNode();
        this.adviceCount = adviceCount;
    }

    public Collection<TrieEntry> searchTermsWithPrefix(String prefix) {
        if (prefix.isEmpty()) {
            return Collections.emptySet();
        }
        TrieNode currentNode = root;
        for (char ch : prefix.toCharArray()) {
            if (currentNode.getChild(ch) != null) {
                currentNode = currentNode.getChild(ch);
            } else {
                return Collections.emptySet();
            }
        }
        return currentNode.relevantEntries;
    }

    public boolean put(TrieEntry entry) {
        if (entry == null || entry.getTerm().isEmpty()) {
            return false;
        }
        TrieNode currentNode = root;
        for (char ch : entry.getTerm().toCharArray()) {
            TrieNode child = currentNode.getChild(ch);
            if (child != null) {
                currentNode = child;
            } else {
                currentNode.childList.put(ch, new TrieNode(currentNode, ch));
                currentNode = currentNode.getChild(ch);
            }
        }
        if (currentNode.entry == null) {
            currentNode.setEntry(entry);
            return true;
        } else {
            return false;
        }
    }

    private class TrieNode{
        char nodeChar;
        TrieEntry entry;
        TrieNode parent;
        TreeSet<TrieEntry> relevantEntries = new TreeSet<>();
        HashMap<Character, TrieNode> childList = new HashMap<>();

        TrieNode() {
            this.parent = null;
        }

        TrieNode(TrieNode parent, char nodeChar) {
            this.parent = parent;
            this.nodeChar = nodeChar;
        }

        TrieNode getChild(char ch){
            if (childList.size() > 0) {
                return childList.get(ch);
            }
            return null;
        }

        void setEntry(TrieEntry entry) {
            this.entry = entry;
            updateRelevantEntries(entry);
        }

        private void updateRelevantEntries(TrieEntry entry) {
            for (TrieNode node = this; node != null; node = node.parent) {
                if (node.relevantEntries.size() < adviceCount) {
                    node.relevantEntries.add(entry);
                } else {
                    if (entry.compareTo(node.relevantEntries.last()) < 0) {
                        node.relevantEntries.pollLast();
                        node.relevantEntries.add(entry);
                    }
                }
            }
        }
    }
}
