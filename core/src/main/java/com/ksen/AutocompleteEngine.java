package com.ksen;

import com.ksen.data.Trie;
import com.ksen.data.TrieEntry;

import java.util.Collection;

public class AutocompleteEngine {

    private Trie dictionary;

    AutocompleteEngine(int adviceCount){
        dictionary = new Trie(adviceCount);
    }

    public void put(String term, int weight){
        dictionary.put(new TrieEntry(term,weight));
    }

    public Collection<TrieEntry> getAdvices(String prefix){
        return dictionary.searchTermsWithPrefix(prefix);
    }
}