package com.ksen;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.ByteBuffer;
import java.nio.channels.SocketChannel;

public class MessageSender {
    private SocketChannel socketChannel;

    public MessageSender(SocketChannel socketChannel) {
        this.socketChannel = socketChannel;
    }

    public void sendFrom(InputStream inputStream) throws IOException {
        try (BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream))) {
            String inputLine;
            while ((inputLine = reader.readLine()) != null) {
                if (inputLine.equals("-1")) {
                    break;
                }
                socketChannel.write(ByteBuffer.wrap(encodeMessage(inputLine)));
            }
            socketChannel.write(ByteBuffer.wrap(encodeMessage("-1")));
        }
    }

    private static byte[] encodeMessage(String message) {

        byte[] messageBytes = new byte[message.length() + 1];
        messageBytes[0] = (byte) message.length();
        System.arraycopy(message.getBytes(), 0, messageBytes, 1, message.length());
        return messageBytes;
    }
}
