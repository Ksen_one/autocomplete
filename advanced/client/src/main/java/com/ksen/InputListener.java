package com.ksen;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.channels.SocketChannel;

public class InputListener implements Runnable {
    private final SocketChannel socketChannel;
    private byte[] inputMessageRemains;

    public InputListener(SocketChannel socketChannel) {
        this.socketChannel = socketChannel;
        inputMessageRemains = new byte[0];
    }

    @Override
    public void run() {
        while (true) {
            try {
                readFromSocketChannel();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    private void readFromSocketChannel() throws IOException {
        ByteBuffer inputBuffer = ByteBuffer.allocate(1024);
        int numberOfBytesRead = socketChannel.read(inputBuffer);
        if (numberOfBytesRead != -1) {
            byte[] inputMessageBytes = new byte[numberOfBytesRead];
            System.arraycopy(inputBuffer.array(), 0, inputMessageBytes, 0, numberOfBytesRead);

            if (inputMessageRemains.length > 0) {
                inputMessageBytes = mergeArrays(inputMessageRemains, inputMessageBytes);
                inputMessageRemains = new byte[0];
            }

            while (inputMessageBytes.length > 0) {
                int firstMessageLength = getFirstMessageLength(inputMessageBytes);
                if (inputMessageBytes.length >= (firstMessageLength + 1)) {
                    byte[] message = extractMessage(inputMessageBytes, firstMessageLength);
                    if (new String(message).equals("-1")) {
                        return;
                    }
                    System.out.println(new String(message));
                    inputMessageBytes = contractArrayFromStart(inputMessageBytes, firstMessageLength + 1);
                } else {
                    inputMessageRemains = inputMessageBytes;
                    break;
                }
            }
        }
    }

    private static int unsignedByte(byte b) {
        return b & 0xFF;
    }

    private static byte[] mergeArrays(byte[] firstArray, byte[] secondArray) {
        byte[] result = new byte[firstArray.length + secondArray.length];
        System.arraycopy(firstArray, 0, result, 0, firstArray.length);
        System.arraycopy(secondArray, 0, result, firstArray.length, secondArray.length);
        return result;
    }

    private static byte[] contractArrayFromStart(byte[] array, int length) {
        byte[] result = new byte[array.length - length];
        System.arraycopy(array, length, result, 0, array.length - length);
        return result;
    }

    private static int getFirstMessageLength(byte[] messagesBytesArray) {
        return messagesBytesArray[0] < 0 ? unsignedByte(messagesBytesArray[0]) : messagesBytesArray[0];
    }

    private static byte[] extractMessage(byte[] inputMessageBytes, int messageLength) {
        byte[] result = new byte[messageLength];
        System.arraycopy(inputMessageBytes, 1, result, 0, messageLength);
        return result;
    }
}
