package com.ksen;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.nio.channels.SocketChannel;

public class Client implements Runnable {
    private InetSocketAddress serverAddress;

    public static void main(String[] args) {
        new Thread(new Client(args[0], Integer.parseInt(args[1]))).start();
    }

    private Client(String hostname, int port) {
        serverAddress = new InetSocketAddress(hostname, port);
    }

    @Override
    public void run() {
        try (SocketChannel socketChannel = SocketChannel.open(serverAddress)) {
            Thread inputListener = new Thread(new InputListener(socketChannel));
            inputListener.start();
            new MessageSender(socketChannel).sendFrom(System.in);
            inputListener.join();
        } catch (IOException | InterruptedException e) {
            e.printStackTrace();
        }
    }
}
