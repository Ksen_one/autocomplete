package com.ksen;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.channels.SocketChannel;
import java.util.stream.Stream;

public class MessageProcessor {
    private String message;

    public MessageProcessor(String message) {
        this.message = message;
    }

    public MessageProcessor process(AutocompleteEngine engine) {
        if (message.equals("-1")) {
            return this;
        }
        if (message.startsWith("get ")){
            message = message.substring(4);
            StringBuilder response = new StringBuilder();
            engine.getAdvices(message).forEach(advice -> response.append(advice.getTerm()).append("\r\n"));
            message = response.toString();
            return this;
        }
        return this;
    }

    public void send(SocketChannel socketChannel) throws IOException {
        socketChannel.write(ByteBuffer.wrap(encodeMessage(message)));
    }

    private byte[] encodeMessage(String message) {
        byte[] result = new byte[message.length()+1];
        result[0] = (byte)message.length();
        System.arraycopy(message.getBytes(), 0, result, 1, message.length());
        return result;
    }
}
