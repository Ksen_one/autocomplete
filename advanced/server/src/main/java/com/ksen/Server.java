package com.ksen;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.nio.channels.SelectionKey;
import java.nio.channels.Selector;
import java.nio.channels.ServerSocketChannel;
import java.nio.channels.SocketChannel;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

public class Server implements Runnable {
    private Selector selector;
    private Map<SocketChannel,byte[]> channelInputMessageRemains;
    private AutocompleteEngine engine;

    public static void main(String[] args) throws Exception {
        new Thread(new Server(Integer.parseInt(args[1]), args[0])).start();
    }

    private Server(int port, String dictionaryPath) throws IOException {
        engine = new AutocompleteEngine(10);
        try {
            Files.lines(Paths.get(dictionaryPath)).forEach(line -> {
                String[] splitedLine = line.split(" ");
                engine.put(splitedLine[0],Integer.parseInt(splitedLine[1]));
            });
        } catch (IOException e) {
            e.printStackTrace();
        }

        channelInputMessageRemains = new HashMap<>();

        this.selector = Selector.open();
        ServerSocketChannel serverChannel = ServerSocketChannel.open();
        serverChannel.configureBlocking(false);
        serverChannel.socket().bind(new InetSocketAddress(port));
        serverChannel.register(this.selector, SelectionKey.OP_ACCEPT);
    }

    @Override
    public void run() {
        try {
            while (true) {
                this.selector.select();
                Iterator keys = this.selector.selectedKeys().iterator();
                while (keys.hasNext()) {
                    SelectionKey key = (SelectionKey) keys.next();
                    keys.remove();

                    if (!key.isValid()) {
                        continue;
                    }
                    if (key.isAcceptable()) {
                        accept(key);
                    } else {
                        if (key.isReadable()) {
                            try {
                                new Reader(key).read();
                            } catch (IOException e) {
                                key.channel().close();
                                key.cancel();
                                e.printStackTrace();
                            }
                        }
                    }
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void accept(SelectionKey key) throws IOException {
        ServerSocketChannel serverChannel = (ServerSocketChannel) key.channel();
        SocketChannel channel = serverChannel.accept();
        channel.configureBlocking(false);
        channelInputMessageRemains.put(channel, new byte[0]);
        channel.register(this.selector, SelectionKey.OP_READ);
    }

    private class Reader {
        private SelectionKey key;

        public Reader(SelectionKey key) {
            this.key = key;
        }

        public void read() throws IOException {
            SocketChannel channel = (SocketChannel) key.channel();
            ByteBuffer buffer = ByteBuffer.allocate(1024);
            int numberOfBytesRead = channel.read(buffer);

            if (numberOfBytesRead == -1) {
                channelInputMessageRemains.remove(channel);
                channel.close();
                key.cancel();
                return;
            }

            byte[] inputMessageBytes = new byte[numberOfBytesRead];
            System.arraycopy(buffer.array(), 0, inputMessageBytes, 0, numberOfBytesRead);

            byte[] inputMessageRemains = channelInputMessageRemains.get(channel);
            if (inputMessageRemains.length > 0) {
                inputMessageBytes = mergeArrays(inputMessageRemains, inputMessageBytes);
                channelInputMessageRemains.put(channel,new byte[0]);
            }

            while (inputMessageBytes.length > 0) {
                int firstMessageLength = inputMessageBytes[0];
                if (inputMessageBytes.length >= (firstMessageLength + 1)) {
                    String message = new String(extractMessage(inputMessageBytes, firstMessageLength));
                    new MessageProcessor(message).process(engine).send(channel);

                    inputMessageBytes = contractArrayFromStart(inputMessageBytes, firstMessageLength + 1);
                } else {
                    channelInputMessageRemains.put(channel, inputMessageBytes);
                    break;
                }
            }
        }

        private byte[] mergeArrays(byte[] firstArray, byte[] secondArray) {
            byte[] result = new byte[firstArray.length + secondArray.length];
            System.arraycopy(firstArray, 0, result, 0, firstArray.length);
            System.arraycopy(secondArray, 0, result, firstArray.length, secondArray.length);
            return result;
        }

        private byte[] contractArrayFromStart(byte[] array, int length) {
            byte[] result = new byte[array.length - length];
            System.arraycopy(array, length, result, 0, array.length - length);
            return result;
        }

        private byte[] extractMessage(byte[] inputMessageBytes, int messageLength) {
            byte[] result = new byte[messageLength];
            System.arraycopy(inputMessageBytes, 1, result, 0, messageLength);
            return result;
        }
    }

}